import React from 'react';

//definisco il layout di base per la navbar

const Navbar = () => {
  return (
    <nav className="navbar navbar-dark bg-dark mb-5">
      <span className="navbar-brand mb-0 h1 mx-auto">musEDIA</span>
    </nav>
  );
};

export default Navbar;
